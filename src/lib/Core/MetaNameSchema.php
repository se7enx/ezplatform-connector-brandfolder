<?php

namespace Contextualcode\Connector\Brandfolder\Core;

use Novactive\Bundle\eZSEOBundle\Core\MetaNameSchema as BaseMetaNameSchema;
use eZ\Publish\Core\FieldType\ImageAsset\Value as ImageAssetValue;
use eZ\Publish\Core\FieldType\Image\Value as ImageValue;

class MetaNameSchema extends BaseMetaNameSchema
{
    protected function handleImageAssetValue(ImageAssetValue $value, $fieldDefinitionIdentifier, $languageCode): string
    {
        if (!$value->destinationContentId) {
            return '';
        }

        if (strpos($value->destinationContentId, 'attachment') !== false || strpos($value->destinationContentId, 'asset') !== false ) {
            return '';
        }

        $content = $this->repository->getContentService()->loadContent($value->destinationContentId);

        foreach ($content->getFields() as $field) {
            if ($field->value instanceof ImageValue) {
                return $this->handleImageValue($field->value, $fieldDefinitionIdentifier, $languageCode);
            }
        }

        return '';
    }
}
