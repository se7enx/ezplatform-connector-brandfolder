<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class UtmSourceExtension extends AbstractExtension
{
    /** @var string */
    private $application_id;

    public function __construct(
        $application_id
    ) {
        $this->application_id = $application_id;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'ibexa_brandfolder_utm_source',
                function () {
                    return $this->application_id;
                },
                ['is_safe' => ['html']]
            ),
        ];
    }
}
