<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;

class BrandfolderSearchType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('query', SearchType::class)
            ->add('section', ChoiceType::class, [
                'required' => false,
                'label' => false,
                'placeholder' => /** @Desc("Section") */ 'ibexa.brandfolder.section',
                'choices' => $options['data']['sections'],
            ]);
    }
}
