<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Variation;

use Ibexa\Platform\Contracts\Connector\Dam\Asset;
use Ibexa\Platform\Contracts\Connector\Dam\Variation\AssetVariation;
use Ibexa\Platform\Contracts\Connector\Dam\Variation\AssetVariationGenerator as AssetVariationGeneratorInterface;
use Ibexa\Platform\Contracts\Connector\Dam\Variation\Transformation;
use Ibexa\Platform\Contracts\Connector\Dam\AssetUri;

final class AssetVariationGenerator implements AssetVariationGeneratorInterface
{
    /** @var \Ibexa\Platform\Contracts\Connector\Dam\Variation\AssetVariationGenerator */
    private $urlBasedVariationGenerator;

    public function __construct(
        AssetVariationGeneratorInterface $urlBasedVariationGenerator
    ) {
        $this->urlBasedVariationGenerator = $urlBasedVariationGenerator;
    }

    public function generate(Asset $asset, Transformation $transformation): AssetVariation
    {
        $location = $asset->getAssetUri()->getPath();

        if (strpos($location, '//cdn') !== false) {
            return $this->urlBasedVariationGenerator->generate($asset, $transformation);
        }

        return new AssetVariation(
            $asset,
            new AssetUri($location),
            $transformation
        );
        
    }
}
