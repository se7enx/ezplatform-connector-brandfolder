<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Query;

use Ibexa\Platform\Contracts\Connector\Dam\Search\Query as BaseQuery;

class Query extends BaseQuery
{
    /** @var string|null */
    protected $section;

    public function __construct(
        string $phrase,
        ?string $section = null
    ) {
        parent::__construct($phrase);
        $this->section = $section;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }
}