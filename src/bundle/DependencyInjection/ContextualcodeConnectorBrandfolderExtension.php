<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Bundle\Connector\Brandfolder\DependencyInjection;

use Contextualcode\Bundle\Connector\Brandfolder\DependencyInjection\Configuration\Parser\Configuration;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Config\Resource\FileResource;

final class ContextualcodeConnectorBrandfolderExtension extends Extension implements PrependExtensionInterface
{
    public function getAlias()
    {
        return 'dam_brandfolder';
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );
        $loader->load('services.yaml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter(
            'ibexa.connector.brandfolder.application_id',
            $config['application_id']
        );
        $container->setParameter(
            'ibexa.connector.brandfolder.secret',
            $config['secret']
        );
        $container->setParameter(
            'ibexa.connector.brandfolder.collections',
            $config['collections']
        );
        $container->setParameter(
            'ibexa.connector.brandfolder.variations',
            $config['variations']
        );
    }

    public function prepend(ContainerBuilder $container): void
    {
        $configs = [
            'ez_field_templates.yaml' => 'ezpublish',
        ];

        foreach ($configs as $fileName => $extensionName) {
            $configFile = __DIR__.'/../Resources/config/'.$fileName;
            $config = Yaml::parse(file_get_contents($configFile));
            $container->prependExtensionConfig($extensionName, $config);
            $container->addResource(new FileResource($configFile));
        }

        $this->prependJMSTranslation($container);
    }

    private function prependJMSTranslation(ContainerBuilder $container): void
    {
        $container->prependExtensionConfig('jms_translation', [
            'configs' => [
                'ezplatform_connector_brandfolder' => [
                    'dirs' => [
                        __DIR__ . '/../../',
                    ],
                    'output_dir' => __DIR__ . '/../Resources/translations/',
                    'output_format' => 'xliff',
                    'excluded_names' => ['*.module.js'],
                    'excluded_dirs' => ['Behat', 'Tests', 'node_modules'],
                    'extractors' => [],
                ],
            ],
        ]);
    }
}
